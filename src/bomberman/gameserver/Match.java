package bomberman.gameserver;

import bomberman.PgBean;
import bomberman.client.MSUserClient;

import java.awt.*;
import java.util.ArrayList;


public class Match {
    private final int rows = 9;
    private final int cols = 9;
    private int uidCounter;
    private int maxPlayers;
    public String mid;
    public int[][] map;
    public ArrayList<PgBean> players;


    public Match(String mid, int maxPlayers) {
        this.mid = mid;
        this.players = new ArrayList<PgBean>();
        this.uidCounter = 5;
        this.maxPlayers = maxPlayers<=4 ? maxPlayers : 4;
        this.map = new int[rows][cols];

        //assegno alle caselle valori casuali tra 0 e 1
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                map[i][j] = (Math.random() < 0.5) ? (byte) 0 : (byte) 1;

        //lascio tre celle libere in ogni angolo
        map[0][0] = 0;
        map[0][1] = 0;
        map[1][0] = 0;
        map[0][cols - 1] = 0;
        map[0][cols - 2] = 0;
        map[1][cols - 1] = 0;
        map[rows - 1][0] = 0;
        map[rows - 1][1] = 0;
        map[rows - 2][0] = 0;
        map[rows - 1][cols - 1] = 0;
        map[rows - 1][cols - 2] = 0;
        map[rows - 2][cols - 1] = 0;


        //alcune caselle hanno blocchi inamovibili
        for (int i = 1; i < rows; i += 2)
            for (int j = 1; j < cols; j += 2)
                map[i][j] = -1;
    }


    public PgBean addPlayer(MSUserClient userStub, String username) {
        PgBean newPlayer = null;
        int uid = uidCounter++;
        int x = 1, y = 0;
        Color color = Color.BLUE;

        switch (players.size()){
            case 0:
                x = 1;
                y = 0;
                color = Color.BLUE;
                break;
            case 1:
                x = cols - 2;
                y = rows - 1;
                color = Color.RED;
                break;
            case 2:
                x = cols - 2;
                y = 0;
                color = Color.GREEN;
                break;
            case 3:
                x = 1;
                y = rows - 1;
                color = Color.YELLOW;
                break;
        }

        newPlayer = new PgBean(userStub, username, uid, x, y, color);

        players.add(newPlayer);

        return newPlayer;
    }


    public void removePlayer(int uid) {
        PgBean toRemove = null;

        for (PgBean player: players)
            if (player.getUid() == uid)
                toRemove = player;

        players.remove(toRemove);
    }


    public int getUID(String username) {
        for (PgBean player: players)
            if (player.getUsername().compareTo(username) == 0)
                return player.getUid();

        return -1;
    }


    public String getUsername(int uid) {
        for (PgBean player: players)
            if (player.getUid() == uid)
                return player.getUsername();

        return null;
    }


    public boolean isFull() {
        return players.size() >= maxPlayers;
    }

}
