package bomberman.gameserver;

import java.io.IOException;
import java.rmi.activation.ActivationException;
import java.util.ArrayList;


/**
 * Interfaccia del server di gioco dedicata agli administrators.
 *
 * @author      Federico Matera
 */
public interface AdminGameServer extends GameServer {

    /**
     * Permette di ottenere la lista delle partite presenti sul server.
     * @return la lista delle partite presenti sul server.
     */
    ArrayList<String> getMatches() throws ActivationException, IOException, ClassNotFoundException;


    /**
     * Permette di creare una nuova partita sul server.
     * @param mid Il nome della nuova partita.
     * @param maxPlayers Il numero massimo di giocatori (valore che va da 2 a 4).
     */
    void createMatch(String mid, int maxPlayers) throws ActivationException, IOException, ClassNotFoundException;


    /**
     * Permette di eliminare una partita dal server.
     * @param mid Il nome della partita da eliminare.
     */
    void deleteMatch(String mid) throws ActivationException, IOException, ClassNotFoundException;
}
