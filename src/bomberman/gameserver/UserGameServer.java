package bomberman.gameserver;

import bomberman.client.MSUserClient;

import java.io.IOException;
import java.rmi.activation.ActivationException;
import java.util.ArrayList;


/**
 * Interfaccia del server di gioco dedicata agli utenti senza privilegi.
 *
 * @author      Federico Matera
 */
public interface UserGameServer extends GameServer {

    /**
     * Permette all'utente di entrare in una nuova partita.
     * Nello specifico trova la prima partita libera disponibile, vi registra il nuovo utente
     * e invia a tutti i giocatori che già erano connessi alla partita (se presenti) i suoi dati,
     * compreso il suo stub (L'utente è un Mobile Server).
     * @param clientStub Lo stub del nuovo utente.
     * @param username Lo username.
     * @return Un ArrayList di generici Object che contiene nell'ordine:
     * <ul>
     *         <li>il nome del match (String);</li>
     *         <li>la mappa di gioco della partita (int[][]);</li>
     *         <li>il bean con i dati del nuovo giocatore impostati dal server(PgBean);</li>
     *         <li>una lista di bean con i dati di tutti i giocatori nella partita (ArrayList<PgBean>).</li>
     * </ul>
     *         I dati vanno castati alla ricezione.
     */
    ArrayList<Object> join(MSUserClient clientStub, String username) throws ActivationException, IOException, ClassNotFoundException;


    /**
     * Permette all'utente di uscire da una partita.
     * Nello specifico elimina l'utente dal match con id mid e avvisa tutti gli altri giocatori
     * di quel match che l'utente uid è uscito.
     * @param uid L'id del giocatore.
     * @param mid L'id del match.
     */
    void leave(int uid, String mid) throws ActivationException, IOException, ClassNotFoundException;

}

