package bomberman.gameserver;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.activation.ActivationException;


/**
 * Interfaccia generale del server di gioco che ne definisce un super-tipo.
 *
 * @author      Federico Matera
 */
public interface GameServer extends Remote {

    /**
     * Fornisce all'utente la possibilità di disconnettersi.
     * Nello specifico questo metodo contatta il server di autenticazione per riferire
     * che l'utente non è più connesso.
     * @param username Lo username.
     */
    void disconnect(String username) throws ActivationException, IOException, ClassNotFoundException;

}

