package bomberman.client;

import bomberman.client.graphics.GameFrame;
import bomberman.gameserver.AdminGameServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Console;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.activation.ActivationException;
import java.util.ArrayList;
import java.util.Vector;


/**
 * Implementazione del client dedicata agli administrators.
 *
 * @author      Federico Matera
 */
public class MAAdminClientImpl implements MAAdminClient, Serializable {
    private AdminGameServer adminGS;
    private String username;
    private JList<String> matchesList;
    private JTextField matchName;


    /**
     * Costruttore del client di gioco invocato dal server prima dell'invio al client.
     * @param ags Stub dell'AdminGameServer.
     * @param username Lo username dell'utente.
     */
    public MAAdminClientImpl(AdminGameServer ags, String username) {
        this.adminGS = ags;
        this.username = username;
    }


    /**
     * {@inheritDoc}
     */
    public void exec() throws ActivationException, IOException, ClassNotFoundException{

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    adminGS.disconnect(username);
                }
                catch (ActivationException ae){
                    System.out.println("AdminClient activation error: " + ae.getMessage());
                    ae.printStackTrace();
                }
                catch (IOException ioe){
                    System.out.println("AdminClient IO error: " + ioe.getMessage());
                    ioe.printStackTrace();
                }
                catch (ClassNotFoundException cnfe){
                    System.out.println("AdminClient class not found error: " + cnfe.getMessage());
                    cnfe.printStackTrace();
                }
            }
        });

        System.out.println("> Connessione admin effettuata.");

        boolean uscita = false;
        Console console = System.console();
        int risposta;
        String nomeMatch;
        int maxPlayers;
        ArrayList<String> matches;

        while (!uscita) {


            matches = adminGS.getMatches();

            if (matches.size()>0) System.out.println("\n> Lista partite presenti sul server:");

            for (int k = 0; k < matches.size(); k++) {
                System.out.println(">> (" + k + ") " + matches.get(k));
            }

            System.out.println("\n> 1) creare una nuova partita");
            if (matches.size()>0) System.out.println("> 2) eliminare una partita");
            System.out.println("> 0) esci.");
            risposta = Integer.parseInt(console.readLine(">>> "));

            switch (risposta) {
                case 1:
                    nomeMatch = console.readLine(">>> Inserire il nome della partita: ");
                    maxPlayers = Integer.parseInt(console.readLine(">>> Inserire il numero massimo di utenti (2-4): "));
                    adminGS.createMatch(nomeMatch, maxPlayers);
                    break;

                case 2:
                    int scelta = Integer.parseInt(console.readLine(">>> Inserire il numero della partita: "));
                    if ((scelta>=0) && (scelta<matches.size())){
                        nomeMatch = matches.get(scelta);
                        adminGS.deleteMatch(nomeMatch);
                    }
                    break;

                case 0:
                    uscita = true;
                    break;
            }
        }
    }

}
