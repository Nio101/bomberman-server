package bomberman.client.graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;


/**
 * Questa classe identifica e disegna una bomba.
 *
 * @author      Federico Matera
 */
public class Bomb extends JComponent {
    private GameFrame window;
    private Point pos;
    public Explosion[] explosions;


    /**
     * Costruttore.
     *
     * @param window il Frame di gioco.
     */
    public Bomb(GameFrame window) {
        this.window = window;
        this.explosions = new Explosion[5];
        final int explDim = 2;

        explosions[0] = new Explosion(window, 0, 1);
        explosions[0].setVisible(false);

        for (int k=1; k<=4; k++) {
            explosions[k] = new Explosion(window, k, explDim);
            explosions[k].setVisible(false);
        }

        setVisible(false);
        setSize(window.CELL_SIZE, window.CELL_SIZE);
    }



    /**
     * Permette di posizionare una bomba in una determinata posizione
     * e renderla visibile; successivamente crea ed esegue un nuovo thread
     * BombTimer che avvia il conto alla rovescia per l'esplosione
     *
     * @param coordinates le coordinate dove è stata lasciata la bomba.
     */
    public void fire(Point coordinates) {
        this.pos = new Point(coordinates);
        Point posInPixel = new Point((int) (window.CELL_SIZE * pos.getX()), (int) (window.CELL_SIZE * pos.getY()));

        window.map[(int) pos.getY()][(int) pos.getX()] = 3;

        setLocation(posInPixel);
        setVisible(true);

        Thread t = new Thread(new BombTimer(window), "explosion");
        t.start();
    }


    /**
     * Metodo che viene chiamato dal thread BombTimer al termine del
     * conto alla rovescia, rende visibili le esplosioni per un certo lasso di tempo.
     */
    private void boom() throws InterruptedException {
        for (int k = 0; k < 5; k++)
            explosions[k].setLocation(pos);


        for (int k = 0; k < 5; k++)
            explosions[k].fire();

        Thread.sleep(500);

        for (int k = 0; k < 5; k++)
            explosions[k].setVisible(false);

        window.map[(int) pos.getY()][(int) pos.getX()] = 0;
    }


    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2.0f));
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(rh);

        Shape bomb = new Ellipse2D.Double(12.5, 20, 25, 25);
        Shape wick = new Line2D.Double(25, 20, 30, 10);

        g2.setColor(Color.BLACK);
        g2.fill(bomb);
        g2.draw(bomb);
        g2.setColor(Color.RED);
        g2.draw(wick);
    }



    /**
     * Questa classe è lanciata su un thread separato e si occupa
     * di gestire il conto alla rovescia e di far esplodere la bomba
     * al termine invocando il metodo boom().
     *
     * @author      Federico Matera
     */
    private class BombTimer implements Runnable {
        GameFrame window;

        public BombTimer(GameFrame window) {
            this.window = window;
        }

        public void run() {
            try {
                Thread.sleep(1000);
                boom();
                setVisible(false);
                window.semBomb = false;
            }
            catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
    }

}

