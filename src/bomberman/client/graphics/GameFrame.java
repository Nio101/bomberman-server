package bomberman.client.graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;
import java.util.ArrayList;


/**
 * Frame di gioco.
 *
 * @author      Federico Matera
 */
public class GameFrame extends JFrame {

    public final int CELL_SIZE = 50;
    public int rows, cols;
    public int[][] map;
    public Block[][] normalBlocks;
    public boolean semBomb;
    public JPanel gamePanel;
    public JTextArea log;
    public ArrayList<Pg> players;
    public Pg me;
    public KeyListener kl;
    public JButton newGame;


    /**
     * Costruttore del frame, partendo dalla matrice di interi
     * che definisce la mappa di gioco la riproduce sul frame
     * mediante l'uso di disegni grafici, inoltre crea la finestra di log
     * e gestisce mediante un KeyListener la pressione dei tasti
     * e la corrispondente azione nel gioco, comunicandola eventualmente al server
     * di gioco.
     *
     * @param newMap La mappa del gioco fornita dal server.
     */
    public GameFrame(int[][] newMap) {
        getContentPane().setLayout(null);
        gamePanel = new JPanel(null);

        this.players = new ArrayList<Pg>();
        this.map = newMap;
        this.semBomb = false;
        cols = map[0].length;
        rows = map.length;
        final int X_SIZE = cols * CELL_SIZE;
        final int Y_SIZE = rows * CELL_SIZE;

        this.getContentPane().setPreferredSize(new Dimension(X_SIZE + 300, Y_SIZE));
        this.setSize(X_SIZE, Y_SIZE);
        gamePanel.setSize(X_SIZE, Y_SIZE);

        normalBlocks = new Block[rows][cols];


        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (map[i][j] == -1) {
                    gamePanel.add(new Block(this, false, j, i));
                }
                if (map[i][j] == 1) {
                    normalBlocks[i][j] = new Block(this, true, j, i);
                    gamePanel.add(normalBlocks[i][j]);
                }
            }
        }


        log = new JTextArea();
        log.setBounds(X_SIZE + 10, 10, 280, 300);
        log.setEditable(false);
        log.setFocusable(false);
        this.setFocusable(false);

        newGame = new JButton("Nuova Partita");
        newGame.setBounds(X_SIZE + 50, 350, 200, 25);
        newGame.setVisible(false);

        this.add(log);
        this.add(newGame);
        this.add(gamePanel);
        gamePanel.setVisible(false);


        this.pack();
        this.setDefaultCloseOperation(GameFrame.EXIT_ON_CLOSE);
        this.setTitle("Bomberman - Federico Matera VR096978");
        this.setBounds(100, 30, X_SIZE + 300, Y_SIZE);
        this.setVisible(true);
        this.setResizable(false);
        this.pack();


        kl = new KeyListener() {
            public void keyPressed(KeyEvent e) {
                try {
                    String direction = null;
                    int x = (int) me.pos.getX();
                    int y = (int) me.pos.getY();

                    if ((e.getKeyCode() == 39) && (x < cols - 1) && (map[y][x + 1] == 0))
                        direction = "right";
                    if ((e.getKeyCode() == 37) && (x > 0) && (map[y][x - 1] == 0))
                        direction = "left";
                    if ((e.getKeyCode() == 38) && (y > 0) && (map[y - 1][x] == 0))
                        direction = "up";
                    if ((e.getKeyCode() == 40) && (y < rows - 1) && (map[y + 1][x] == 0))
                        direction = "down";

                    if (direction != null)
                        for (Pg pg: players)
                            pg.userStub.moves(me.uid, direction);

                    if (e.getKeyCode() == 32) {
                        if (!semBomb) {
                            semBomb = true;

                            for (Pg pg: players)
                                pg.userStub.fires(me.uid, me.pos);
                        }
                    }
                }
                catch (RemoteException re) {
                    System.out.println("GameFrame remote error: non riesco a contattare gli altri utenti per riferire la tua mossa.");
                    re.printStackTrace();
                }
            }

            public void keyTyped(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }
        };

        gamePanel.addKeyListener(kl);
    }


}
