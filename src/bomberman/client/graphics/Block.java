package bomberman.client.graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;


/**
 * Questa classe identifica e disegna un blocco.
 *
 * @author      Federico Matera
 */
public class Block extends JComponent {
    private GameFrame window;
    private int colA, rowA;        //absolute (pixel)
    private int colR, rowR;    //relative (matrix)
    private boolean movable;


    /**
     * Costruttore.
     *
     * @param window il Frame di gioco.
     * @param movable true se il blocco si può distruggere, false altrimenti.
     * @param x ascissa relativa alla tabella.
     * @param y ordinata relativa alla tabella.
     */
    public Block(GameFrame window, boolean movable, int x, int y) {
        this.window = window;
        this.colA = window.CELL_SIZE * x;
        this.rowA = window.CELL_SIZE * y;
        this.colR = x;
        this.rowR = y;
        this.movable = movable;

        setBounds(colA, rowA, window.CELL_SIZE, window.CELL_SIZE);
    }


    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2.0f));
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(rh);

        RoundRectangle2D block = new RoundRectangle2D.Double();
        block.setRoundRect(2.5, 2.5, 45, 45, 20, 20);

        if (movable)
            g2.setColor(Color.LIGHT_GRAY);
        else
            g2.setColor(new Color(146, 111, 91));

        g2.fill(block);
        g2.setColor(Color.BLACK);
        g2.draw(block);
    }
}
