package bomberman.client.graphics;

import javax.swing.*;
import java.awt.*;
import java.rmi.RemoteException;


/**
 * Questa classe identifica e disegna una serie di esplosioni
 * in una particolare direzione, per un'esplosione completa servono
 * 5 direzioni: il centro più destra, sinistra, alto e basso.
 *
 * @author      Federico Matera
 */
public class Explosion extends JComponent {
    private GameFrame window;
    private Point pos;
    private int direction;
    private Explosion next;


    /**
     * Costruttore.
     *
     * @param window il Frame di gioco.
     * @param direction direzione del raggio dell'esplosione rappresentato, può essere:
     * <ul>
     *         <li>0: centro;</li>
     *         <li>1: destra;</li>
     *         <li>2: sinistra;</li>
     *         <li>3: su;</li>
     *         <li>4: giu'.</li>
     * </ul>
     * @param counter numero di esplosioni consecutive nella direzione data.
     */
    public Explosion(GameFrame window, int direction, int counter) {
        this.window = window;
        this.direction = direction;
        this.pos = null;

        if (counter > 1)
            this.next = new Explosion(window, direction, counter - 1);
        else
            this.next = null;

        this.setSize(window.CELL_SIZE, window.CELL_SIZE);
        window.gamePanel.add(this);
    }


    /**
     * Data la coordinata della bomba setta le esplosioni rappresentate
     * nella posizione corretta in base alla direzione dichiarata nel costruttore.
     *
     * @param coordinates coordinate della bomba.
     */
    @Override
    public void setLocation(Point coordinates) {
        int x = (int) coordinates.getX();
        int y = (int) coordinates.getY();
        Point posInPixel;

        switch (direction) {
            case 0:
                this.pos = new Point(x, y);
                break;
            case 1:
                this.pos = new Point(x + 1, y);
                break;
            case 2:
                this.pos = new Point(x - 1, y);
                break;
            case 3:
                this.pos = new Point(x, y - 1);
                break;
            case 4:
                this.pos = new Point(x, y + 1);
                break;
        }

        if ((pos.getX() >= 0) && (pos.getX() < window.cols) && (pos.getY() >= 0) && (pos.getY() < window.rows)) {
            posInPixel = new Point((int) (window.CELL_SIZE * this.pos.getX()), (int) (window.CELL_SIZE * this.pos.getY()));
            super.setLocation((int) posInPixel.getX(), (int) posInPixel.getY());

            if (next != null)
                next.setLocation(pos);
        } else {
            this.pos = null;
        }
    }


    /**
     * Rende visibili le esplosioni e,
     * se nella posizione di un'esplosione c'è l'utente locale,
     * viene inviato a tutti i giocatori l'avviso che l'utente locale è morto.
     */
    public void fire() {
        try {
            if (pos != null) {
                int x = (int) pos.getX();
                int y = (int) pos.getY();
                int value = window.map[y][x];

                if (value != -1) {
                    if (value == 1) {
                        window.normalBlocks[y][x].setVisible(false);
                        window.map[y][x] = 0;
                    } else if (value == window.me.uid) {
                        for (int k = 1; k < window.players.size(); k++)
                            window.players.get(k).userStub.dies(window.me.uid);

                        window.players.get(0).userStub.dies(window.me.uid);
                    } else {
                        if (next != null)
                            next.fire();
                    }

                    super.setVisible(true);
                }
            }
        }
        catch (RemoteException re) {
            System.out.println("Explosion remote error: non riesco a contattare gli altri utenti per riferire la tua morte.");
            re.printStackTrace();
        }
    }


    /**
     * Rende visibile o meno l'esplosione e tutte le esplosioni
     * che seguono nella stessa direzione.
     *
     * @param flag true per rendere visibile, false altrimenti.
     */
    @Override
    public void setVisible(boolean flag) {
        super.setVisible(flag);
        if (next != null)
            next.setVisible(flag);
    }


    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2.0f));
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(rh);

        Polygon pol = new Polygon();
        pol.addPoint(25, 10);
        pol.addPoint(30, 15);
        pol.addPoint(35, 13);
        pol.addPoint(40, 20);
        pol.addPoint(45, 15);
        pol.addPoint(47, 25);
        pol.addPoint(40, 30);
        pol.addPoint(37, 35);
        pol.addPoint(30, 35);
        pol.addPoint(25, 45);
        pol.addPoint(20, 40);
        pol.addPoint(15, 35);
        pol.addPoint(10, 40);
        pol.addPoint(5, 30);
        pol.addPoint(10, 25);
        pol.addPoint(10, 10);
        pol.addPoint(20, 15);

        g2.setColor(Color.ORANGE);
        g2.fill(pol);
        g2.setColor(Color.BLACK);
        g2.draw(pol);
    }
}
