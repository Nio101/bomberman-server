package bomberman.client.graphics;

import bomberman.PgBean;
import bomberman.client.MSUserClient;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;


/**
 * Questa classe identifica e disegna un giocatore e ne gestisce i movimenti.
 *
 * @author      Federico Matera
 */
public class Pg extends JComponent {
    private GameFrame window;
    public MSUserClient userStub;
    public String username;
    public int uid;
    public int rowA, colA;    //absolute (pixel)
    public Point pos, posInPixel;
    public Bomb bomb;
    private Color color;


    /**
     * Costruttore.
     *
     * @param window Il Frame di gioco.
     * @param bean Il bean contenente tutti i dati del giocatore.
     */
    public Pg(GameFrame window, PgBean bean) {
        this.window = window;
        this.userStub = bean.getStub();
        this.username = bean.getUsername();
        this.uid = bean.getUid();
        this.pos = bean.getLocation();
        this.color = bean.getColor();
        this.posInPixel = new Point((int) (window.CELL_SIZE * pos.getX()), (int) (window.CELL_SIZE * pos.getY()));
        this.bomb = new Bomb(window);

        window.map[(int) pos.getY()][(int) pos.getX()] = uid;
        setBounds((int) posInPixel.getX(), (int) posInPixel.getY(), window.CELL_SIZE, window.CELL_SIZE);
        this.window.gamePanel.add(this);
        this.window.gamePanel.add(bomb);
    }


    /**
     * Permette di muovere il personaggio nel frame.
     *
     * @param direction La direzione in cui muoversi, può essere
     * <ul>
     *         <li>"right";</li>
     *         <li>"left";</li>
     *         <li>"up";</li>
     *         <li>"down".</li>
     * </ul>
     */
    public void move(String direction) {
        if (window.map[(int) pos.getY()][(int) pos.getX()] == uid)
            window.map[(int) pos.getY()][(int) pos.getX()] = 0;

        if (direction.compareTo("right") == 0)
            pos.translate(1, 0);
        if (direction.compareTo("left") == 0)
            pos.translate(-1, 0);
        if (direction.compareTo("up") == 0)
            pos.translate(0, -1);
        if (direction.compareTo("down") == 0)
            pos.translate(0, 1);


        posInPixel.setLocation(window.CELL_SIZE * (int) pos.getX(), window.CELL_SIZE * (int) pos.getY());

        window.map[(int) pos.getY()][(int) pos.getX()] = uid;
        setBounds((int) posInPixel.getX(), (int) posInPixel.getY(), window.CELL_SIZE, window.CELL_SIZE);
    }



    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2.0f));
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHints(rh);

        Shape testa = new Ellipse2D.Double(18, 2, 15, 15);
        Shape busto = new Line2D.Double(25, 17, 25, 35);
        Shape gamba1 = new Line2D.Double(25, 35, 15, 49);
        Shape gamba2 = new Line2D.Double(25, 35, 35, 49);
        Shape braccia = new Line2D.Double(10, 25, 40, 25);

        g2.setColor(color);
        g2.draw(testa);
        g2.draw(busto);
        g2.draw(gamba1);
        g2.draw(gamba2);
        g2.draw(braccia);
    }
}
