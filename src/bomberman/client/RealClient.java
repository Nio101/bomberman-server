package bomberman.client;

import bomberman.login.Bootstrap;

import java.io.Console;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.activation.ActivationException;


/**
 * Client vero presente sul server e scaricato in fase preliminare
 * attraverso il meccanismo di bootstrap.
 *
 * @author Federico Matera
 */
public class RealClient implements Runnable, Serializable {

    private Bootstrap authServer;


    /**
     * Costruttore senza parametri, serve se si scarica questa classe
     * con RMIClassLoader.
     */
    public RealClient() {
        this.authServer = null;
    }


    /**
     * Costruttore invocato dal server prima di inviare questa classe al client.
     * @param authServer Lo stub del server di autenticazione
     */
    public RealClient(Bootstrap authServer) {
        this.authServer = authServer;
    }


    /**
     * Implementazione del metodo run, permette all'utente di fare il login e,
     * nel caso fosse andato a buon fine, avviare la classe ritornata dal server di autenticazione.
     */
    public void run() {
        String username;
        String password;
        MobClient cli;

        try {
            Console console = System.console();
            username = console.readLine("username: ");
            password = new String(console.readPassword("password: "));

            System.out.println("> Connessione al server in corso, attendere...");
            cli = authServer.login(username, password);
        }
        catch (RemoteException re){
            System.out.println("% Server di autenticazione irraggiungibile.");
            re.printStackTrace();
            return;
        }


        try {
            cli.exec();
        }
        catch (NullPointerException npe){
            System.out.println("> Accesso vietato, password errata oppure l'utente risulta già connesso.");
        }
        catch (ActivationException ae){
            System.out.println("% Client activation error: " + ae.getMessage());
            ae.printStackTrace();

            // se c'è qualche errore di rete provo a disconnettermi presso il server
            // così da liberare il nickname
            try {
                authServer.logout(username);
            }
            catch (RemoteException re) {
                System.out.println("> Disconnessione automatica non riuscita.");
                re.printStackTrace();
            }
        }
        catch (IOException ioe){
            System.out.println("% Client IO error: " + ioe.getMessage());
            ioe.printStackTrace();

            try {
                authServer.logout(username);
            }
            catch (RemoteException re) {
                System.out.println("> Disconnessione automatica non riuscita.");
                re.printStackTrace();
            }
        }
        catch (ClassNotFoundException cnfe){
            System.out.println("% Client class not found error: " + cnfe.getMessage());
            cnfe.printStackTrace();

            try {
                authServer.logout(username);
            }
            catch (RemoteException re) {
                System.out.println("> Disconnessione automatica non riuscita.");
                re.printStackTrace();
            }
        }
    }
}
