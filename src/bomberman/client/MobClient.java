package bomberman.client;

import java.io.IOException;
import java.rmi.activation.ActivationException;


/**
 * Interfaccia generale del client che ne definisce un super-tipo.
 *
 * @author      Federico Matera
 */
public interface MobClient {

    /**
     * Contiene il codice che verrà eseguito.
     */
    void exec() throws ActivationException, IOException, ClassNotFoundException;

}
