package bomberman.client;

import bomberman.PgBean;
import bomberman.client.graphics.GameFrame;
import bomberman.client.graphics.Pg;
import bomberman.gameserver.GameServer;
import bomberman.gameserver.UserGameServer;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.activation.ActivationException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.net.UnknownHostException;


/**
 * Implementazione del client dedicata agli utenti senza privilegi.
 *
 * @author      Federico Matera
 */
public class MSUserClientImpl implements MSUserClient, Serializable {
    private UserGameServer userGS;
    private MSUserClient ucStub;
    private String username;
    private String mid;
    private GameFrame frame;


    /**
     * Costruttore del client di gioco invocato dal server prima dell'invio al client.
     * @param ugs Stub del UserGameServer.
     * @param username Lo username dell'utente.
     */
    public MSUserClientImpl(GameServer ugs, String username) throws RemoteException {
        this.userGS = (UserGameServer) ugs;
        this.username = username;

        System.setProperty("javax.net.ssl.trustStorePassword", "123456");
    }


    /**
     * {@inheritDoc}
     */
    public void export(int port) throws RemoteException {
		try{
			//resetto l'hostname altrimenti mantiene quello del server visto che questa
			//classe è stata istanziata la
			System.setProperty("java.rmi.server.hostname", java.net.InetAddress.getLocalHost().getHostAddress());
		}
		catch (UnknownHostException uhe){
			System.out.println("# Host del client sconosciuto.");
		}
		
        ucStub = (MSUserClient) UnicastRemoteObject.exportObject(this, port);
        System.out.println("# UserClient Mobile server is exported in UnicastRemoteObject.");
        /* se arriva qua entrambi i server hanno gli stub funzionanti dell'altro
         * quindi quando questo metodo termina posso considerare la connessione stabilita */

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    userGS.leave(frame.me.uid, mid);
                    userGS.disconnect(username);
                }
                catch (ActivationException ae){
                    System.out.println("shutdownHook activation error: " + ae.getMessage());
                    ae.printStackTrace();
                }
                catch (IOException ioe){
                    System.out.println("shutdownHook IO error: " + ioe.getMessage());
                    ioe.printStackTrace();
                }
                catch (ClassNotFoundException cnfe){
                    System.out.println("shutdownHook class not found error: " + cnfe.getMessage());
                    cnfe.printStackTrace();
                }
            }
        });
     }


    /**
     * {@inheritDoc}
     */
    public void joins(PgBean newPlayer) throws RemoteException {
        Pg newPg = new Pg(frame, newPlayer);
        frame.players.add(newPg);
        frame.gamePanel.add(newPg);

        frame.log.append("> entrato nuovo utente:\n  -username: " + username + ";\n  -uid: " + frame.me.uid + "\n");
        frame.gamePanel.setVisible(true);
        frame.gamePanel.setFocusable(true);
        frame.gamePanel.requestFocusInWindow();
    }


    /**
     * {@inheritDoc}
     */
    public void leaves(int uid) throws RemoteException {
            //se sono io ad essere disconnesso ricomincio tutto
            if (uid == 0) {
                frame.log.append("> tutti gli utenti si sono disconnessi.\n");
                frame.gamePanel.removeKeyListener(frame.kl);
                frame.gamePanel.setEnabled(false);
                frame.newGame.setVisible(true);
            } else {
                Pg toRemove = null;

                for (Pg player : frame.players)
                    if (player.uid == uid)
                        toRemove = player;

                if (toRemove != null){
                    toRemove.setVisible(false);
                    frame.players.remove(toRemove);
                }

                frame.log.append("> " + uid + " si è disconnesso.\n");
            }
    }


    /**
     * {@inheritDoc}
     */
    public void moves(int uid, String direction) throws RemoteException {
        for (Pg player : frame.players)
            if (player.uid == uid)
                player.move(direction);
    }


    /**
     * {@inheritDoc}
     */
    public void fires(int uid, Point coordinates) throws RemoteException {
        for (Pg player : frame.players)
            if (player.uid == uid)
                player.bomb.fire(coordinates);
    }


    /**
     * {@inheritDoc}
     */
    public void dies(int uid) throws RemoteException {
        Pg playerToDelete = null;

        for (Pg player : frame.players)
            if (player.uid == uid){
                playerToDelete = player;
                frame.log.append("> muore " + player.username + "\n");
            }

        if (playerToDelete != null){
            playerToDelete.setVisible(false);
            frame.players.remove(playerToDelete);
        }

        if (uid == frame.me.uid) {
            frame.me.setVisible(false);
            frame.gamePanel.removeKeyListener(frame.kl);
        }

        if (frame.players.size() == 1) {
            frame.log.append("> vince " + frame.players.get(0).username + "\n");
            frame.gamePanel.removeKeyListener(frame.kl);
            frame.gamePanel.setEnabled(false);
            frame.newGame.setVisible(true);
        }
    }


    /**
     * {@inheritDoc}
     */
    public void exec() throws ActivationException, IOException, ClassNotFoundException {
        if (ucStub == null)
            export(10011);  //10011

        ArrayList<Object> data = userGS.join(ucStub, username);

        if (data == null)
            System.out.println("> Non ci sono partite libere.");
        else {
            mid = (String) data.get(0);
            int[][] map = (int[][]) data.get(1);
            PgBean myBean = (PgBean) data.get(2);
            ArrayList tempPgs = (ArrayList) data.get(3);

            frame = new GameFrame(map);
            frame.log.append("> inizia una nuova partita.\n");
            frame.log.append("> username: " + username + "\n");
            frame.log.append("> entro nella partita: " + mid + "\n");
            frame.log.append("> uid assegnato: " + myBean.getUid() + "\n");
            frame.log.append("> in attesa di altri giocatori...\n");

            frame.me = new Pg(frame, myBean);
            frame.players.add(frame.me);

            for (int k = 0; k < tempPgs.size(); k++) {
                PgBean pg = (PgBean) tempPgs.get(k);
                if (pg.getUid() != myBean.getUid())
                    joins(pg);
            }


            frame.newGame.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent event) {
                    try {
                        userGS.leave(frame.me.uid, mid);
                        frame.setVisible(false);
                        frame = null;
                        exec();
                    }
                    catch (ActivationException ae){
                        System.out.println("% newGame button activation error: " + ae.getMessage());
                        ae.printStackTrace();
                    }
                    catch (IOException ioe){
                        System.out.println("% newGame button IO error: " + ioe.getMessage());
                        ioe.printStackTrace();
                    }
                    catch (ClassNotFoundException cnfe){
                        System.out.println("% newGame button class not found error: " + cnfe.getMessage());
                        cnfe.printStackTrace();
                    }
                }
            });
        }
    }

}
