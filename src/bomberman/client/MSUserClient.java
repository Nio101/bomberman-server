package bomberman.client;

import bomberman.PgBean;

import java.awt.*;
import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * Interfaccia del client dedicata agli utenti senza privilegi.
 *
 * @author      Federico Matera
 */
public interface MSUserClient extends Remote, MobClient {

    /**
     * Permette al client di essere esportato come UnicastRemoteObject,
     * rendendolo così un Mobile Server.
     * @param port La porta di esportazione
     */
    void export(int port) throws RemoteException;


    /**
     * Metodo remoto invocato dal server quando un altro utente entra nella partita attuale.
     * @param newPlayer Un bean contenente tutti i dati del giocatore nuovo.
     */
    void joins(PgBean newPlayer) throws RemoteException;


    /**
     * Metodo remoto invocato dal server quando un altro utente abbandona la partita.
     * @param uid Id dell'utente che ha abbandonato.
     */
    void leaves(int uid) throws RemoteException;


    /**
     * Metodo remoto invocato dagli altri utenti quando si muovono
     * per dichiarare la loro attuale posizione.
     * @param uid Id dell'utente che si è mosso.
     * @param direction direzione in cui si è mosso scelta tra
     * <ul>
     *         <li>"right";</li>
     *         <li>"left";</li>
     *         <li>"up";</li>
     *         <li>"down".</li>
     * </ul>
     */
    void moves(int uid, String direction) throws RemoteException;


    /**
     * Metodo remoto invocato dagli altri utenti quando innescano una bomba.
     * @param uid Id dell'utente che si è mosso.
     * @param coordinates Posizione nella tabella in cui la bomba è stata lasciata.
     */
    void fires(int uid, Point coordinates) throws RemoteException;


    /**
     * Metodo remoto invocato dagli altri utenti quando muoiono.
     * @param uid Id dell'utente che è morto.
     */
    void dies(int uid) throws RemoteException;
}
