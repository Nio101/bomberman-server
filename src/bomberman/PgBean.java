package bomberman;

import bomberman.client.MSUserClient;

import java.awt.*;
import java.io.Serializable;



/**
 * PgBean è un bean contenente i dati di un giocatore da inviare via rete.
 *
 * @author      Federico Matera
 */
public class PgBean implements Serializable {
    private MSUserClient userStub;
    private String username;
    private Point coordinates;
    private Color color;
    private int uid;


    /**
     * Costruttore del bean.
     * @param userStub Lo stub.
     * @param username L'username.
     * @param uid L'uid.
     * @param x La posizione sulle ascisse.
     * @param y La posizione sulle ordinate.
     * @param color Il colore del personaggio.
     */
    public PgBean(MSUserClient userStub, String username, int uid, int x, int y, Color color) {
        this.userStub = userStub;
        this.username = username;
        this.uid = uid;
        this.coordinates = new Point(x, y);
        this.color = color;
    }


    /**
     * Restituisce lo stub del giocatore.
     * @return	lo stub del giocatore.
     */
    public MSUserClient getStub() {
        return userStub;
    }


    /**
     * Restituisce l'username del giocatore.
     * @return	l'username del giocatore.
     */
    public String getUsername() {
        return username;
    }


    /**
     * Restituisce l'uid del giocatore.
     * @return	l'uid del giocatore.
     */
    public int getUid() {
        return uid;
    }


    /**
     * Restituisce la posizione del giocatore.
     * @return	la posizione del giocatore.
     */
    public Point getLocation() {
        return coordinates;
    }


    /**
     * Restituisce il colore del giocatore.
     * @return	il colore del giocatore.
     */
    public Color getColor() {
        return color;
    }


}
