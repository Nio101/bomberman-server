package bomberman;

import bomberman.gameserver.GameServer;
import bomberman.login.AuthServer;
import bomberman.login.Bootstrap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.MarshalledObject;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.activation.*;
import java.util.Properties;



/**
 * Setup è la classe principale dell'applicazione lato server e
 * si occupa di eseguire il setup del sistema e di tutti i server in ascolto.
 *
 * @author      Federico Matera
 */
public class Setup {

    public static void main(String args[]) {
         String serverIP;
         int httpPort;
         int rmiregistryPort;
         int cosnamingPort;
         int rmidPort;
         String codebase;
         String gsPolicy;
         String gameServerClass;

        try {
            System.setSecurityManager(new SecurityManager());

            if (args.length == 0) {
                throw new IllegalArgumentException("la sintassi corretta è \"java Server TUOIP\"");
            }

            serverIP = args[0];
            httpPort = 8000;
            rmidPort = 1098;
            rmiregistryPort = 1099;
            cosnamingPort = 1100;
            codebase = "http://" + serverIP + ":" + httpPort + "/";
            gsPolicy = "policy.all";
            gameServerClass = "bomberman.gameserver.GameServerImpl";


            System.setProperty("java.rmi.server.codebase", codebase);
            System.setProperty("java.rmi.server.hostname", serverIP);
            System.setProperty("java.rmi.dgc.leaseValue", "31000");
            //System.setProperty("java.rmi.dgc.checkInterval", "15000");

			
			
			
			/* Esecuzione e registrazione di "AuthServer" */
            Bootstrap authServer = new AuthServer();
			
			
			/* Bind sul registro RMI */
            Properties prop1 = new Properties();
            prop1.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.rmi.registry.RegistryContextFactory");
            prop1.put(Context.PROVIDER_URL, "rmi://127.0.0.1:" + rmiregistryPort);
            InitialContext cxt1 = new InitialContext(prop1);
            cxt1.rebind("AuthServer", authServer);
            System.out.println("# AuthServer registrato sul Registro RMI.");
			
			/* Bind sul Cos Naming */
            Properties prop2 = new Properties();
            prop2.put("java.naming.factory.initial", "com.sun.jndi.cosnaming.CNCtxFactory");
            prop2.put("java.naming.provider.url", "iiop://127.0.0.1:" + cosnamingPort);
            InitialContext cxt2 = new InitialContext(prop2);
            cxt2.rebind("AuthServer", authServer);
            System.out.println("# AuthServer registrato su COS Naming.");
			
			
			
			/* Esecuzione e registrazione dei "GameServer" */
            Properties prop = new Properties();
            prop.put("java.security.policy", gsPolicy);
            prop.put("bomberman.impl.codebase", codebase);
            prop.put("java.rmi.server.hostname", serverIP);

            ActivationGroupDesc groupDesc1 = new ActivationGroupDesc(prop, null);
            ActivationGroupID groupID1 = ActivationGroup.getSystem().registerGroup(groupDesc1);
            ActivationDesc actDesc1 = new ActivationDesc(groupID1, gameServerClass, codebase, new MarshalledObject<Bootstrap>(authServer));

            GameServer gs = (GameServer) Activatable.register(actDesc1);
            Naming.rebind("//:" + rmidPort + "/gameserver", gs);
            System.out.println("# GameServer registrato in RMID.");

            System.out.println("# Tutti i server sono operativi.");
        }
        catch (RemoteException re) {
            System.out.println("Setup remote error: " + re.getMessage());
            re.printStackTrace();
        }
        catch (ActivationException ae) {
            System.out.println("Setup activation error: " + ae.getMessage());
            ae.printStackTrace();
        }
        catch (NamingException ne) {
            System.out.println("Setup naming error: " + ne.getMessage());
            ne.printStackTrace();
        }
        catch (MalformedURLException mue) {
            System.out.println("Setup Malformed URL error: " + mue.getMessage());
            mue.printStackTrace();
        }
        catch (IOException ioe) {
            System.out.println("Setup IO error: " + ioe.getMessage());
            ioe.printStackTrace();
        }
        finally{
            return;
        }

    }

}
