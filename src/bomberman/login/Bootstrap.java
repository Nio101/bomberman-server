package bomberman.login;

import bomberman.client.MobClient;

import java.rmi.MarshalledObject;
import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * Interfaccia del server di autenticazione.
 *
 * @author      Federico Matera
 */
public interface Bootstrap extends Remote {

    /**
     * Restituisce Il RealClient al client minimale.
     * @return	RealClient.
     */
    MarshalledObject getClient() throws RemoteException;


    /**
     * Permette ad un client di eseguire il login mediante l'uso di un database SQL,
     * se l'utente non è ancora inserito nel database lo registra con la password scelta.
     * @param username Lo username.
     * @param password La password.
     * @return	un'istanza del mobile agent MAAdminClientImpl se l'utente loggato è un admin;
     *          un'istanza de-esportata del mobile server MSUserClientImpl se l'utente loggato non è un admin;
     *          null se il login è fallito.
     */
    MobClient login(String username, String password) throws RemoteException;


    /**
     * Disconnette un utente, cioè lo elimina dalla lista degli utenti connessi;
     * finchè un utente non si disconnette non gli sarà possibile riconnettersi una seconda volta.
     * @param username Lo username da disconnettere.
     */
    void logout(String username) throws RemoteException;
} 
